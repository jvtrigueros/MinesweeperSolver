package com.josetrigueros.test;

import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.josetrigueros.minesweeper.robot.MinesweeperRobot;

public class TestingRobot
{
    private static String filename = "minesweeper.dat";
    /**
     * @param args
     */
    public static void main( String[] args )
    {   
        // Loading the robot or initializing from scratch.
        MinesweeperRobot robot = loadRobot();
        if( robot == null )
            robot = initializeRobot();
        
        /** Playground **/
        robot.scan(0, 10);
        for ( int i = 0; i < 10; i++ )
        {
            robot.move(i, i);
        }
        /** Playground **/
        
        // Saving the robot, coords etc.
        saveRobot(robot);
    }

    private static MinesweeperRobot initializeRobot()
    {
        Point horizontalStartingPoint = new Point(1144, 310);
        Point verticalStartingPoint = new Point(1161, 290);
        MinesweeperRobot robot = null;
        try
        {
            robot = new MinesweeperRobot(horizontalStartingPoint,verticalStartingPoint);
            robot.calibrate();
        } catch ( AWTException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return robot;
    }

    private static boolean saveRobot( MinesweeperRobot robot )
    {
        boolean wasSaved = false;
        
        if ( robot != null )
        {
            try
            {
                ObjectOutputStream out= new ObjectOutputStream(new FileOutputStream(filename));
                out.writeObject(robot);
                out.close();
                wasSaved = true;
                
            } catch ( FileNotFoundException e )
            {
                System.out.println("File not found, cool don't worry keep it coding.");
            } catch ( IOException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return wasSaved;
    }
    
    private static MinesweeperRobot loadRobot()
    {
        MinesweeperRobot loadedRobot = null;
        try
        {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            loadedRobot = ( MinesweeperRobot ) in.readObject();
            in.close();
        } catch ( FileNotFoundException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch ( IOException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch ( ClassNotFoundException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return loadedRobot;
    }
    
    private static void robotTestBoard( Point[][] boardPoints, Robot robot )
    {
        for ( int i = 0; i < 3; i++ )
        {
            for ( int j = 0; j < 2; j++ )
            {
                Point currentPoint = boardPoints[i][j];
                robot.mouseMove(currentPoint.x, currentPoint.y);
                // Initialize Robot and move to starting position.        
                robot.mousePress(InputEvent.BUTTON1_MASK);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);
                robot.delay(50);
            }
        }
        
    }


}
