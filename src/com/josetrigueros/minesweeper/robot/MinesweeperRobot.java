/**
 * 
 */
package com.josetrigueros.minesweeper.robot;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Jose Trigueros
 * 
 */
public class MinesweeperRobot extends Robot implements Serializable
{
    /**
     * serialVersionUID given to it by Eclipse
     */
    private static final long serialVersionUID = -5466977556943301247L;
    private final int HORIZONTAL = -1;
    private final int VERTICAL = 1;
    private int THRESHOLD = 0;

    private final String PRESSED = "p";

    private Point horizontalStartingPoint;
    private Point verticalStartingPoint;
    private Point[][] pointMatrix;
    private String[][] mineMatrix;

    /**
     * @throws AWTException
     */
    public MinesweeperRobot(Point horizontalStartingPoint, Point verticalStartingPoint) throws AWTException
    {
        this.horizontalStartingPoint = horizontalStartingPoint;
        this.verticalStartingPoint = verticalStartingPoint;
    }

    /**
     * @param screen
     * @throws AWTException
     */
    public MinesweeperRobot(GraphicsDevice screen) throws AWTException
    {
        super(screen);
    }

    /**
     * TODO Rename method, has crappy non-representative name Will click on square given the x,y
     * coordinates
     * 
     * @param x
     * @param y
     */
    public void clickSquare( int x, int y )
    {
        // Determine if that square has been pressed or not
        String mineLocation = mineMatrix[x][y];

        if ( mineLocation == null ) // Has not been pressed, press it.
        {
            // Get the actual location on the screen that needs to be clicked
            move(x, y);
            click();

            mineLocation = new String(PRESSED);
        } else if ( mineLocation.equals(PRESSED) ) // Has been pressed, do not press it.
        {
            // TODO Do something.
        }
    }

    public void calibrate()
    {
        // TODO Need to figure out what to do about the scanning range.
        int scanningRange = 570;
        THRESHOLD = 18;
        ArrayList<Integer> horizontalPoints = calibrate(horizontalStartingPoint, scanningRange, HORIZONTAL);

        // TODO Need to figure out what to do about the scanning range.
        scanningRange = 460;
        THRESHOLD = 5;
        ArrayList<Integer> verticalPoints = calibrate(verticalStartingPoint, scanningRange, VERTICAL);

        // Generate point and mine matrix so that we know where the squares are
        pointMatrix = generatePointMatrix(horizontalPoints, verticalPoints);
        mineMatrix = new String[pointMatrix.length][pointMatrix[0].length];
    }

    private Point[][] generatePointMatrix( ArrayList<Integer> horizontalPoints, ArrayList<Integer> verticalPoints )
    {
        int numCols = horizontalPoints.size();
        int numRows = verticalPoints.size();
        Point[][] board = new Point[numCols][numRows];

        for ( int y = 0; y < numRows; y++ )
        {
            for ( int x = 0; x < numCols; x++ )
            {
                board[x][y] = new Point(horizontalPoints.get(x), verticalPoints.get(y));
            }
        }
        return board;
    }

    private ArrayList<Integer> calibrate( Point startingPoint, int scanningRange, int orientation )
    {
        int samplingRate = 3;

        int xCoord = startingPoint.x;
        int yCoord = startingPoint.y;

        // Initialize Robot and move to starting position.
        this.mouseMove(xCoord, yCoord);
        this.mousePress(InputEvent.BUTTON1_MASK);
        this.mouseRelease(InputEvent.BUTTON1_MASK);

        // Final list of points
        ArrayList<Integer> pointList = new ArrayList<Integer>();

        // Populating Queues with empty data
        Queue<Integer> colorQueue = new LinkedList<Integer>(Arrays.asList(new Integer[]
            { 0, 0, 0 }));
        Queue<Integer> pointQueue = new LinkedList<Integer>(Arrays.asList(new Integer[]
            { 0, 0, 0 }));

        // TODO Make it scan until the pattern repeats again and make it a do-while loop.
        // Scan the window
        for ( int i = 1; i < scanningRange; i += samplingRate )
        {
            // Depending on orientation, increment the correct axis.
            if ( orientation == HORIZONTAL )
                xCoord = startingPoint.x + i;
            else
                yCoord = startingPoint.y + i;

            // Set new position
            this.mouseMove(xCoord, yCoord);

            // Get new currentPixel
            Color currentPixel = this.getPixelColor(xCoord, yCoord);

            // Calculate Y component, as in YIQ
            int y = rgbToY(currentPixel);

            /*
             * Calculating edges
             */
            // Pushing current Y value and Point
            colorQueue.add(y);

            // Using the ternary operator to determine which axis is the one we care about.
            pointQueue.add((orientation == HORIZONTAL) ? xCoord : yCoord);

            // Pop out oldest Y value and Point
            colorQueue.remove();
            pointQueue.remove();

            // Determine if the points find an edge.
            Integer[] colorArray = colorQueue.toArray(new Integer[colorQueue.size()]);
            if ( isEdge(colorArray) )
            {
                Integer[] pointArray = pointQueue.toArray(new Integer[pointQueue.size()]);
                pointList.add(pointArray[1]);
            }
        }

        // ArrayList<Integer> normalizedList = normalizedPointList( pointList );
        //
        // // DEBUG This is done for debugging purposes. On the second arg, give opposite axis.
        // pointVerifier(normalizedList, ( orientation == HORIZONTAL ) ? yCoord : xCoord ,
        // orientation, robot);

        return normalizePointList(pointList);
    }

    private int rgbToY( Color pixel )
    {
        return (int) (0.299 * pixel.getRed() + 0.587 * pixel.getGreen() + 0.114 * pixel.getBlue());
    }

    private ArrayList<Integer> normalizePointList( ArrayList<Integer> pointList )
    {
        ArrayList<Integer> normalizedList = new ArrayList<Integer>();
        for ( int i = 0; i < pointList.size() - 1; i++ )
        {
            normalizedList.add((int) ((pointList.get(i) + pointList.get(i + 1)) / 2.0));
        }
        return normalizedList;
    }

    /**
     * Determines if the set of three values are considered an edge the following two constraints
     * <ol>
     * <li>Must be concave up, meaning the first and last value must be higher than the middle
     * value.</li>
     * <li>The difference between the first and middle point must be greater than the
     * <code>THRESHOLD</code> value.</li>
     * </ol>
     * 
     * @param pointList
     * @return
     */
    private boolean isEdge( Integer[] colorArray )
    {
        if ( colorArray[0] > colorArray[1] && colorArray[2] > colorArray[1] )
        {
            int delta = Math.abs(colorArray[0] - colorArray[1]);
            if ( delta > THRESHOLD ) return true;
        }

        return false;
    }

    /**
     * Simple wrapper around the mouse move, press, release commands.
     * 
     * @param coordinate
     */
    private void click()
    {
        this.mousePress(InputEvent.BUTTON1_MASK);
        this.mouseRelease(InputEvent.BUTTON1_MASK);
        delay(100);
    }

    /**
     * Wrapper for mouseMove that takes a Point as opposed to two arguments
     * 
     * @param x
     *            TODO
     * @param y
     *            TODO
     */
    public void move( int x, int y )
    {
        Point coordinate = pointMatrix[x][y];
        this.mouseMove(coordinate.x, coordinate.y);
    }

    public void scan( int x, int y )
    {
        int edgeThreshold = 5;
        int maxScan = 10;
        Point midpoint = pointMatrix[x][y];
        ArrayList<Integer> signal = new ArrayList<Integer>();

        // Scan from the midpoint backwards until hitting an edge
        for ( int i = midpoint.x; midpoint.x - i < maxScan; i-- )
        {
            mouseMove(i, midpoint.y);
            int intensityValue = rgbToY(getPixelColor(i, midpoint.y));
            if ( intensityValue < edgeThreshold )
                break;
            else
                signal.add(intensityValue);
        }

        // Reverse signal
        Collections.reverse(signal);

        // Scan from the midpoint forward until hitting an edge
        for ( int i = midpoint.x + 1; i - midpoint.x < maxScan; i++ )
        {
            mouseMove(i, midpoint.y);
            int intensityValue = rgbToY(getPixelColor(i, midpoint.y));
            if ( intensityValue < edgeThreshold )
                break;
            else
                signal.add(intensityValue);
        }

        printArrayList(signal);
    }

    /**
     * DEBUG Temporary, prints ArrayList of ?
     * 
     * @param list
     */
    private void printArrayList( ArrayList<?> list )
    {
        for ( Object o : list )
        {
            System.out.println(o.toString());
        }
    }

    /**
     * DEBUG Testing function that will probably be deleted or will remain unused.
     * 
     * @param pointList
     * @param stableAxisValue
     * @param orientation
     * @param robot
     */
    @SuppressWarnings("unused")
    private void pointVerifier( ArrayList<Integer> pointList, int stableAxisValue, int orientation, Robot robot )
    {
        int xCoord, yCoord;

        // Iterate through all the points in the edgeList
        for ( int i = 0; i < pointList.size(); i++ )
        {
            if ( orientation == HORIZONTAL )
            {
                xCoord = pointList.get(i);
                yCoord = stableAxisValue;
            } else
            {
                yCoord = pointList.get(i);
                xCoord = stableAxisValue;
            }
            this.mouseMove(xCoord, yCoord);
            this.delay(300);
        }
    }
}
